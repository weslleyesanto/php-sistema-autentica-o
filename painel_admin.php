<?php
//session_start(); 
include_once('include\config.php'); 
verificarSessionUsuario(1, $conn); 

$TABELA = "tb_log l";
$INNER = " INNER JOIN tb_usuario u ON l.id_usuario = u.id_usuario";
$GROUP_BY = " GROUP BY u.id_usuario";
$PARAM = " , COUNT(*) as num_acessos ";
//$ORDER_BY = " ORDER BY l.dt_log ASC";

                            //$conn, $TABELA, $PARAM, $WHERE, $INNER, $GROUP_BY, $DEBUG
$execute_select = select($conn, $TABELA, $PARAM, false, $INNER, $GROUP_BY);

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Painel Administrativo</title>

</head>

<body>

    <div id="wrapper">

     <?php include_once('include\header.php') ?>

     <div id="page-wrapper">

        <div class="container-fluid">

            <div class="table-responsive" id="listaUsuarios">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Data/Hora</th>
                            <th>Usuario</th>
                            <th>IP</th>
                            <th>Número de acessos</th>
                            <th>Logado?</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  
                        if($execute_select->rowCount() > 0){
                            foreach($execute_select as $row):

                                $ic_logado = $row["ic_logado"];

                                $value = $ic_logado == 1 ? "Sim" : "Não";
                            ?>
                            <tr data-id="<?= $row["id_usuario"] ?>">
                                <td><?= trataData($row["dt_log"], 3) ?></td>
                                <td><?= utf8_encode($row["nm_usuario"]) ?></td>
                                <td><?= $row["ip"] ?></td>
                                <td><?= $row["num_acessos"] ?></td>
                                <td><button type="button"><?=$value?></button></td>
                                <td>
                                    <a href="#" title="Desconectar usuário" id="desconectarUsuario">Desconectar</a>
                                </td>
                            </tr>
                            <?php
                            endforeach;
                        }else{
                            ?>
                            <tr>
                                <td colspan="6">Nenhum registro encontrado!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php include_once('include\js.php') ?>

</body>

</html>
