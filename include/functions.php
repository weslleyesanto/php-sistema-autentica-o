<?php
//Arquivo criado para criar funções que seram usadas dentro do sistema

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
    	$ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
    	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
    	$ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


function verificarSessionUsuario($n, $conn){

	//_print_r($_SESSION, true);

	$LOGIN = "login.php";
	$LOGOUT = "logout.php";
	
	if(isset($_SESSION) AND isset($_SESSION['autenticado']) AND $_SESSION['autenticado'] == "S"){
		
		$TABELA = "tb_usuario ";
		$WHERE = " WHERE id_usuario = " .$_SESSION['id_usuario'] . " AND " . " ic_usuario = " . $n;

								//$conn, $TABELA, $PARAM, $WHERE, $INNER, $GROUP_BY, $DEBUG
		$execute_select = select($conn, $TABELA, false, $WHERE, false, false);

		if($execute_select->rowCount() > 0){

			foreach($execute_select as $row) {
				$nivel = $row["ic_usuario"];
				$ic_logado = $row["ic_logado"];
			}
			//FINAL FOREACH DO RETORNO DO SELECT

			if($ic_logado != 1){//VERIFICA NO BANCO SE USUáRIO ESTÁ LOGADO
				header("location:{$LOGOUT}");
				exit();
			}
			//FINAL IF IC_LOGADO
		}else{
			//verificarSession();
			header("location:{$LOGOUT}");
			exit();
		}
		//FINAL VERIFICA SE ENCONTROU USUARIO
	}else{
		header("location:{$LOGOUT}");
		exit();
	}
	//FINAL VERIFICA SESSION AUTENTICADO IGUAL A S
}


function acessosUsuario($id_usuario, $conn){

	$LOGOUT = "logout.php";

	$TABELA = "tb_log";
	$WHERE = " WHERE id_usuario = " . $id_usuario;
	$PARAM = " ,COUNT(*) as num_acessos ";

	//echo('<pre>'); print_r($_SESSION); echo('</pre>'); die();

							//$conn, $TABELA, $PARAM, $WHERE, $INNER, $GROUP_BY, $DEBUG
	$execute_select = select($conn, $TABELA, $PARAM, $WHERE, false, false);

	if($execute_select->rowCount() > 0){
		foreach ($execute_select as $row) {
			$num_acessos = $row["num_acessos"];
		}
		echo($num_acessos);
	}else{
		header("location:{$LOGOUT}");
	}
}
//FINAL FUNCTION ACESSOS USUARIO

function verificarSession(){

	if(isset($_SESSION) AND isset($_SESSION["autenticado"]) AND $_SESSION["autenticado"] == "S"){

		$nivel = $_SESSION['nivel'];

		if($nivel == 1){
			header("location:painel_admin.php");
		}elseif($nivel == 2){
			header("location:painel_usuario.php");
		}
	}

}
//FINAL FUNCTION VERIFICA SESSION

function _print_r($array, $die = false){
	
	$var .= '<pre>'; 
	$var .= print_r($array); 
	$var .= '</pre>'; 

	if($die){
		$var .= die();	
	}

	return $var;

}

function trataData($string, $tipo = 0) {
	if ($string && empty($string) == false) {
		switch ($tipo) {
			case 1: 
			$ex = explode('/', $string);
			return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			break;
			case 2:
			$ex = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string)); 
			return $ex[2] . '/' . $ex[1] . '/' . $ex[0];
			break;
			case 3:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '/' . $ex_data[1] . '/' . $ex_data[0] . ' ' . $horario;
			break;
			case 4:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '-' . $ex_data[1] . '-' . $ex_data[0] . ' ' . $horario;
			break;
			default:
			$ex = explode('-', $string);
			return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			break;
		}
	}
}

function limpar($string){

	$string = trim($string);
	$string =str_replace("'","",$string);//aqui retira aspas simples <'>
	$string =str_replace("\\","",$string);//aqui retira barra invertida<\\>
	$string =str_replace("UNION","",$string);//aqui retiro  o comando UNION <UNION>

	$banlist = array(" insert", " select", " update", " delete", " distinct", " having", " truncate", "replace"," handler", " like", " as ", "or ", "procedure ", " limit", "order by", "group by", " asc", " desc","'","union all", "=", "'", "(", ")", "<", ">", " update", "-shutdown",  "--", "'", "#", "$", "%", "¨", "&", "'or'1'='1'", "--", " insert", " drop", "xp_", "*", " and");
	// ---------------------------------------------
	if(preg_match("/[a-zA-Z0-9]+/", $string)){
		$string = trim(str_replace($banlist,'', strtolower($string)));
	}

	return $string;

}

function validaEmail($email) {
	$conta = "^[a-zA-Z0-9\._-]+@";
	$domino = "[a-zA-Z0-9\._-]+.";
	$extensao = "([a-zA-Z]{2,4})$";

	$pattern = $conta.$domino.$extensao;

	if (ereg($pattern, $email))
		return true;
	else
		return false;
}

function select($conn, $TABELA, $PARAM = false, $WHERE, $INNER = false, $GROUP_BY = false, $DEBUG = false){
		//VERIFICA SE PARAM É FALSE'
	if($PARAM == false){
		$PARAM = "";
	}

		//VERIFICA SE INNER É FALSE'
	if($INNER == false){
		$INNER = "";
	}

		//VERIFICA SE GROUP_BY É FALSE'
	if ($GROUP_BY == false){
		$GROUP_BY = "";
	}

		//MONTANDO QUERY'
	$QUERY = "SELECT * " . $PARAM . " FROM " . $TABELA . $INNER . $WHERE . $GROUP_BY;

	if($DEBUG){
		die('###SELECT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function update($conn, $TABELA, $SET, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'UPDATE ' . $TABELA . $SET .$WHERE;

	if($DEBUG){
		die('###UPDATE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function insert($conn, $TABELA, $PARAM, $DEBUG = false){

	$QUERY = 'INSERT INTO ' . $TABELA . $PARAM;

	if($DEBUG){
		die('###INSERT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}
?>