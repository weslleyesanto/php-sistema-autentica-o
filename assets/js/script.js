var script = (function() {
	var S = {}, winH, winW;

	S.setDHTML = function() {
		$(function() {


		});

		$('#listaUsuarios').on('click', '#desconectarUsuario', function() {
			
			var r = confirm("Deseja realmente desconectar esse usuario?");

			if (r) {

				var id_usuario = $(this).parents('tr').attr('data-id');

				S.logoutUsuario(id_usuario);
				return false;
			} 
			
		});

	};

	S.logoutUsuario = function(id_usuario){

		var data = {id_usuario: id_usuario};

		//console.log(id_usuario); //return false;

		$.post('desconectar_usuario.php',data, function(ret){

			//console.log(ret); return false;

			if(ret.res == "ok"){
				alert(ret.msg); 
				window.location.href = ret.url;
				return false;
			}else if(ret.res == "error"){
				alert(ret.msg); return false;	
			}


		}, 'json');

	}


	$(function() {
		S.setDHTML();
	});

	return S;
})();