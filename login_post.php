<?php 
include_once('include\config.php'); 

$LOGIN = 'login.php';

if(isset($_POST) AND isset($_POST['email']) AND isset($_POST['senha'])){

	$retorno = array();

	if($_POST['email'] != "" AND $_POST['senha'] != ""){

		$email = limpar($_POST['email']);
		$senha = limpar($_POST['senha']);

		$TABELA = "tb_usuario ";
		$WHERE = " WHERE nm_email = '{$email}' AND ds_senha = MD5('{$senha}') AND ic_status = 1";

		$execute = select($conn, $TABELA, false, $WHERE, false, false);

		if($execute->rowCount() > 0){
			
			foreach($execute as $row) {
				$id_usuario = $row["id_usuario"];
				$nm_usuario = $row["nm_usuario"];
				$nm_email = $row["nm_email"];
				$ds_senha = $row["ds_senha"];
				$nivel = $row["ic_usuario"];
				$ic_logado = 1;
				$ip = getRealIpAddr();
			}
			//FINAL FOREACH EXECUTA QUERY

			$TABELA = "tb_usuario ";
			$SET = " SET ic_logado = 1";
			$WHERE = " WHERE id_usuario = {$id_usuario} AND ic_status = 1";
			
			$execute_update = update($conn, $TABELA, $SET, $WHERE);

			if($execute_update->rowCount() > 0){

				$TABELA = "tb_log ";
				$PARAM = "(id_usuario, ip) VALUES ('{$id_usuario}', '{$ip}')";

				$execute_insert = insert($conn, $TABELA, $PARAM);

				if($execute_insert){

					//Criar as variaveis de sessão
					$_SESSION['autenticado'] = "S";
					$_SESSION['id_usuario'] = $id_usuario;
					$_SESSION['nm_usuario'] = $nm_usuario;
					$_SESSION['nm_email'] = $nm_email;
					$_SESSION['ic_logado'] = $ic_logado;
					$_SESSION['nivel'] = $nivel;
					
					if($nivel == 1){
						$url = 'painel_admin.php';
					}else if($nivel == 2){
						$url = 'painel_usuario.php';
					}else{
						$url = 'index.php';
					}
					//FINAL VERIFICAÇÃO DE NÍVEL
					$retorno = array('res' => 'ok', 'msg' => 'Usuário encontrado!', 'url'  => $url);	
				}else{
					$retorno = array('res' => 'error', 'msg' => 'Erro ao inserir LOG!', 'url'  => $LOGIN);	
				}
				//FINAL VERIFICA SE INSERIU LOG

			}else{
				$retorno = array('res' => 'error', 'msg' => 'Erro ao alterar status logado usuário!', 'url'  => $LOGIN);
			}
			//FINAL VERIFICA SE TROCOU STATUS PARA LOGADO

		}else{
			$retorno = array('res' => 'error', 'msg' => 'Usuário NÃO encontrado!', 'url'  => $LOGIN);
		}
		//FINAL VERIFICA SE ENCONTROU O USUARIO
	}else{

		if($_POST['email'] == ""){
			echo('Preencha o campo e-mail!'); 
		}
		//FINAL VERIFICA E-MAIL VAZIO

		if(!validaEmail($_POST['email'])){
			echo('Preenchimento do campo e-mail incorreta!'); 	
		}
		//FINAL VALIDA E-MAIL

		if($_POST['senha'] == ""){
			echo('Preencha o campo senha!'); 
		}
		//FINAL VERIFICA SENHA IGUAL A VAZIO
		die();
		//header("location:{$LOGIN}");
	}
	//FINAL VERIFICAÇÃO E-MAIL E SENHA VAZIO

	echo(json_encode($retorno));
}else{
	header("location:{$LOGIN}");
}
//FINAL VERIFICA POST


?>