<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

</head>

<body>

    <div id="wrapper">

     <?php include_once('include\header.php') ?>

     <div id="page-wrapper">

        <div class="container-fluid">
            Index Page
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php') ?>

</body>

</html>
