<?php 
include_once('include\config.php'); 
verificarSessionUsuario(2, $conn); 
?> 

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Painel Usuário</title>

</head>

<body>

    <div id="wrapper">

     <?php include_once('include\header.php') ?>

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1 class="page-header">Painel Usuario</h1>
            
            <h2>Hello, <?= utf8_encode($_SESSION["nm_usuario"]) ?>!</h2>
            <p>Você acessou <?= acessosUsuario($_SESSION["id_usuario"], $conn); ?> vezes o sistema!</p>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php') ?>

</body>

</html>
